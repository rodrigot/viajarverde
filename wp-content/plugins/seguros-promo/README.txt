=== Seguros Promo ===
Contributors: (plugins2xt)
Donate link: https://segurospromo.com.br
Tags: segurospromo, seguros, promo, afiliado, viagem
Requires at least: 3.4.2
Tested up to: 5.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Stable tag: 1.6.7

O Plugin Seguros Promo permite aos blogs enriquecer o conteúdo dos seus textos com a inclusão de preços de seguros viagem que são atualizados constantemente pelas nossas APIs.

Além de deixar o conteúdo mais rico e melhorar a indexação das páginas com conteúdos atualizados, nossos afiliados também são REMUNERADOS pelas vendas provenientes dos seus blogs.

Não perca tempo, instale nosso plugin, seja um PARCEIRO PROMO e começe a lucrar como muitos outros já fazem.


== Description ==

O Plugin Seguros Promo permite aos blogs enriquecer o conteúdo dos seus textos com a inclusão de preços de seguros viagem que são atualizados constantemente pelas nossas APIs.

Além de deixar o conteúdo mais rico e melhorar a indexação das páginas com conteúdos atualizados, nossos afiliados também são REMUNERADOS pelas vendas provenientes dos seus blogs.

Não perca tempo, instale nosso plugin, seja um PARCEIRO PROMO e começe a lucrar como muitos outros já fazem.


== Installation ==

* É importante que você tenha o cadastro em nosso programa de afiliados para ter acesso ao seu Uid.
* Faça o upload do arquivo .Zip de dentro do próprio WordPress, configure o seu Uid de afiliado e comece a usar.
* No seu editor de post/página aparecerá um link para você adicionar o conteúdo de seguros viagem em seu texto.

== Changelog ==

**1.6.7**

Tested up to 5.4-RC1-47424 – Changed nofollow rel link to sponsored

**1.6.6**

Fix Jquery Error Admin

**1.6.5**

Fix Jquery Error Admin

**1.6.4**

Added AMP CSS

**1.6.3**

Fix Version Update

**1.6.2**

Fix Logos

**1.6.1**

Fix permissão de leitura Logos

**1.6.0**

Ajuste logos

**1.5.9**

- Ajuste logo ITA

**1.5.8**

- Alteração de logo

**1.5.7**

- Adicionado escopo em algumas classes css

**1.5.6**

- Ajuste nos texto dos boxes e atributos das tags img

**1.5.5**

- Ajuste na cor de texto para Iphone e tamanho dos boxes

**1.5.0**

- Adicionado suporte aos blocos do Gutenberg
- Botão para alternar visibilidade da logo está disponível no menu do bloco

**1.5.1**

- Descrição do plugin atualizada

**1.5.3**

- Correção de bugs