<?php
/*-----------------------------------------------------------------------------------*/
/*	Define Theme Vars
/*-----------------------------------------------------------------------------------*/

define('THEME_DIR', trailingslashit(get_template_directory()));
define('THEME_URI', trailingslashit(get_template_directory_uri()));
define('THEME_NAME', 'Voice');
define('THEME_SLUG', 'voice');
define('THEME_VERSION', '1.2.1');
define('THEME_OPTIONS', 'vce_settings');
define('JS_URI', THEME_URI . 'js');
define('CSS_URI', THEME_URI . 'css');
define('IMG_DIR', THEME_DIR . 'images');
define('IMG_URI', THEME_URI . 'images');

if (!isset($content_width)) {
	$content_width = 730;
}


/*-----------------------------------------------------------------------------------*/
/*	After Theme Setup
/*-----------------------------------------------------------------------------------*/

add_action('after_setup_theme', 'vce_theme_setup');

function vce_theme_setup()
{

	/* Load frontend scripts and styles */
	add_action('wp_enqueue_scripts', 'vce_load_scripts');

	/* Load admin scripts and styles */
	add_action('admin_enqueue_scripts', 'vce_load_admin_scripts');

	/* Register sidebars */
	add_action('widgets_init', 'vce_register_sidebars');

	/* Register menus */
	add_action('init', 'vce_register_menus');

	/* Register widgets */
	add_action('widgets_init', 'vce_register_widgets');

	/* Add thumbnails support */
	add_theme_support('post-thumbnails');

	add_image_size('newlayc', 375, 260, true);
	add_image_size('newlayd', 84, 63, true);

	/* Add image sizes */
	$image_sizes = vce_get_image_sizes();
	$image_sizes_opt = vce_get_option('image_sizes');
	foreach ($image_sizes as $id => $size) {
		if (isset($image_sizes_opt[$id]) && $image_sizes_opt[$id]) {
			add_image_size($id, $size['w'], $size['h'], $size['crop']);
		}
	}

	/* Add post formats support */
	add_theme_support('post-formats', array(
		'gallery', 'image', 'video'
	));

	/* Support for HTML5 */
	add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery'));

	/* Automatic Feed Links */
	add_theme_support('automatic-feed-links');
}


/* Load frontend styles */
function vce_load_styles()
{

	//Load fonts
	$fonts = vce_generate_font_links();
	if (!empty($fonts)) {
		foreach ($fonts as $k => $font) {
			wp_register_style('vce_font_' . $k, $font, false, THEME_VERSION, 'screen');
			wp_enqueue_style('vce_font_' . $k);
		}
	}
	wp_enqueue_style('reset', "https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css");

	//Load main css file
	wp_enqueue_style('vce_style', THEME_URI . 'style.min.css', false, '', 'screen, print');
	// wp_enqueue_style( 'vce_style' );

	//Enqueue font awsm icons if css is not already included via plugin
	if (!wp_style_is('mks_shortcodes_fntawsm_css', 'enqueued')) {
		wp_register_style('vce_font_awesome', CSS_URI . '/font-awesome.min.css', false, THEME_VERSION, 'screen');
		wp_enqueue_style('vce_font_awesome');
	}


	//Load RTL css
	if (vce_get_option('rtl_mode')) {
		wp_register_style('vce_rtl', CSS_URI . '/rtl.css', array('vce_style'), THEME_VERSION, 'screen');
		wp_enqueue_style('vce_rtl');
	}

	//Append dynamic css
	$vce_dynamic_css = vce_generate_dynamic_css();
	wp_add_inline_style('vce_style', $vce_dynamic_css);

	wp_enqueue_style('bootstrap', CSS_URI . "/bootstrap.min.css");
	wp_enqueue_style('Montserrat', "//fonts.googleapis.com/css?family=Montserrat:400,600,700");
	wp_enqueue_style('Open-Sans', "//fonts.googleapis.com/css?family=Open+Sans:400,600,700");
	wp_enqueue_style('custom', CSS_URI . '/custom.min.css', array(), '3.0.0');
	wp_enqueue_style('fontello', CSS_URI . '/fontello.css');

	//Load responsive css
	if (vce_get_option('responsive_mode')) {
		wp_register_style('vce_responsive', CSS_URI . '/responsive.css', array('vce_style'), THEME_VERSION, 'screen');
		wp_enqueue_style('vce_responsive');
	}
}


/* Load frontend scripts */
function vce_load_scripts()
{

	vce_load_styles();
	// wp_dequeue_script('jquery');
	// wp_dequeue_script('jquery-migrate');
	wp_deregister_script('jquery');
	wp_deregister_script('jquery-migrate');
	wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js', array(), '1.11.3', true);
	wp_enqueue_script('jquery-migrate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js', array('jquery'), '1.2.1', true);
	wp_enqueue_script('ajax', JS_URI . '/ajax.js', array('jquery'), '1.0.0', true);
	wp_localize_script('ajax', 'MyAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
	wp_enqueue_script('vce_images_loaded', JS_URI . '/imagesloaded.pkgd.min.js', array('jquery'), THEME_VERSION, true);
	wp_enqueue_script('vce_owl_slider', JS_URI . '/owl.carousel.min.js', array('jquery'), THEME_VERSION, true);
	wp_enqueue_script('vce_affix', JS_URI . '/affix.js', array('jquery'), THEME_VERSION, true);
	wp_enqueue_script('vce_match_height', JS_URI . '/jquery.matchHeight.js', array('jquery'), THEME_VERSION, true);
	wp_enqueue_script('vce_fitvid', JS_URI . '/jquery.fitvids.js', array('jquery'), THEME_VERSION, true);
	wp_enqueue_script('vce_responsivenav', JS_URI . '/jquery.sidr.min.js', array('jquery'), THEME_VERSION, true);
	wp_enqueue_script('bootstrap', JS_URI . '/bootstrap.min.js', array('jquery'), THEME_VERSION, true);



	if (is_singular()) {
		wp_enqueue_script('vce_magnific_popup', JS_URI . '/jquery.magnific-popup.min.js', array('jquery'), THEME_VERSION, true);
	}

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

	wp_enqueue_script('vce_custom', JS_URI . '/custom.js', array('jquery', 'slick', 'fancybox'), THEME_VERSION, true);

	$vce_js_settings = vce_get_js_settings();
	wp_localize_script('vce_custom', 'vce_js_settings', $vce_js_settings);


	wp_enqueue_style('slick', JS_URI . '/slick/slick.css');
	wp_enqueue_style('slick-theme', JS_URI . '/slick/slick-theme.css');
	wp_enqueue_script('slick', JS_URI . '/slick/slick.min.js', array('jquery'), THEME_VERSION, true);
	wp_enqueue_style('fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css');
	wp_enqueue_script('fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js', array('jquery'), THEME_VERSION, true);
}

/* Load admin scripts and styles */
function vce_load_admin_scripts()
{

	global $pagenow, $typenow;

	//Load amdin css
	wp_register_style('vce_admin_css', CSS_URI . '/admin.css', false, THEME_VERSION, 'screen');
	wp_enqueue_style('vce_admin_css');

	//Load category JS
	if ($pagenow == 'edit-tags.php' && isset($_GET['taxonomy']) && $_GET['taxonomy'] == 'category') {
		wp_enqueue_style('wp-color-picker');
		wp_enqueue_script('vce_category', JS_URI . '/metaboxes-category.js', array('jquery', 'wp-color-picker'), THEME_VERSION);
	}

	//Load post & page metaboxes css and js
	if ($pagenow == 'post.php' || $pagenow == 'post-new.php') {
		if ($typenow == 'post') {
			wp_enqueue_script('vce_post_metaboxes', JS_URI . '/metaboxes-post.js', array('jquery'), THEME_VERSION);
		} elseif ($typenow == 'page') {
			wp_enqueue_script('vce_post_metaboxes', JS_URI . '/metaboxes-page.js', array('jquery'), THEME_VERSION);
		}
	}
}

/* Support localization */
load_theme_textdomain(THEME_SLUG, THEME_DIR . '/languages');


/*-----------------------------------------------------------------------------------*/
/*	Theme Includes
/*-----------------------------------------------------------------------------------*/


/* Helpers and utility functions */
require_once 'include/helpers.php';

/* Menus */
require_once 'include/menus.php';

/* Sidebars */
require_once 'include/sidebars.php';

/* Widgets */
require_once 'include/widgets.php';

/* Add custom metaboxes for standard post types */
require_once 'include/metaboxes.php';

/* Snippets (modify/add some special features to this theme) */
require_once 'include/snippets.php';

/* Simple mega menu solution */
require_once 'include/mega-menu.php';

/* Include AJAX action handlers */
require_once 'include/ajax.php';

/* Include plugins (required or recommended for this theme) */
require_once 'include/plugins.php';

/* Theme Options */
require_once 'include/options.php';



function exclude_widget_categories($args)
{
	$exclude = "1,41,42,43"; // The IDs of the excluding categories
	$args["exclude"] = $exclude;
	return $args;
}
add_filter("widget_categories_args", "exclude_widget_categories");

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js($src)
{
	if (strpos($src, 'ver=' . get_bloginfo('version')))
		$src = remove_query_arg('ver', $src);
	return $src;
}
add_filter('style_loader_src', 'vc_remove_wp_ver_css_js', 9999);
add_filter('script_loader_src', 'vc_remove_wp_ver_css_js', 9999);

//Page Slug Body Class
function add_slug_body_class($classes)
{
	global $post;
	if (isset($post)) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter('body_class', 'add_slug_body_class');

function custom_excerpt_length($length)
{
	return 20;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

add_filter('template_include', 'var_template_include', 1000);
function var_template_include($t)
{
	$GLOBALS['current_theme_template'] = basename($t);
	return $t;
}


function new_subcategory_hierarchy()
{
	$category = get_queried_object();

	$parent_id = $category->category_parent;

	$templates = array();

	if ($parent_id == 0) {
		// Use default values from get_category_template()
		$templates[] = "category-{$category->slug}.php";
		$templates[] = "category-{$category->term_id}.php";
		$templates[] = 'category.php';
	} else {
		// Create replacement $templates array
		$parent = get_category($parent_id);

		// Current first
		$templates[] = "category-{$category->slug}.php";
		$templates[] = "category-{$category->term_id}.php";

		// Parent second
		$templates[] = "category-{$parent->slug}.php";
		$templates[] = "category-{$parent->term_id}.php";
		$templates[] = 'category.php';
	}
	return locate_template($templates);
}
add_filter('category_template', 'new_subcategory_hierarchy');


function get_current_template($echo = false)
{
	if (!isset($GLOBALS['current_theme_template']))
		return false;
	if ($echo)
		echo $GLOBALS['current_theme_template'];
	else
		return $GLOBALS['current_theme_template'];
}

function update_category_widget_args($args)
{
	$args['hide_empty'] = 0;
	$cats = get_categories(array('child_of' => 41, 'hide_empty' => 0));
	$colunacat = array();
	foreach ($cats as $cat)
		array_push($colunacat, $cat->term_id);
	$colunacatStr = implode($colunacat, array(','));
	$args["exclude"] = '1,62,41,' . $colunacatStr; //semcategoria, rapidinhas, colunas
	return $args;
}
add_filter('widget_categories_args', 'update_category_widget_args');


function hw_custom_body_class($classes)
{
	$ancestors = get_ancestors(
		get_queried_object_id(),
		get_queried_object()->taxonomy
	);

	if (empty($ancestors)) {
		return $classes;
	}

	foreach ($ancestors as $ancestor) {
		$term = get_term($ancestor, get_queried_object()->taxonomy);
		$classes[] = esc_attr("$term->taxonomy-$term->slug");
	}
	return $classes;
}
add_filter('body_class', 'hw_custom_body_class');


// add_filter( 'post_gallery', 'my_post_gallery', 10, 2 );
// function my_post_gallery( $output, $attr) {
//     global $post, $wp_locale;

//     static $instance = 0;
//     $instance++;

//     // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
//     if ( isset( $attr['orderby'] ) ) {
//         $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
//         if ( !$attr['orderby'] )
//             unset( $attr['orderby'] );
//     }

//     extract(shortcode_atts(array(
//         'order'      => 'ASC',
//         'orderby'    => 'menu_order ID',
//         'id'         => $post->ID,
//         'itemtag'    => 'dl',
//         'icontag'    => 'dt',
//         'captiontag' => 'dd',
//         'columns'    => 3,
//         'size'       => 'thumbnail',
//         'include'    => '',
//         'exclude'    => ''
//     ), $attr));

//     $id = intval($id);
//     if ( 'RAND' == $order )
//         $orderby = 'none';

//     if ( !empty($include) ) {
//         $include = preg_replace( '/[^0-9,]+/', '', $include );
//         $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

//         $attachments = array();
//         foreach ( $_attachments as $key => $val ) {
//             $attachments[$val->ID] = $_attachments[$key];
//         }
//     } elseif ( !empty($exclude) ) {
//         $exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
//         $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
//     } else {
//         $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
//     }

//     if ( empty($attachments) )
//         return '';

//     if ( is_feed() ) {
//         $output = "\n";
//         foreach ( $attachments as $att_id => $attachment )
//             $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
//         return $output;
//     }

//     $itemtag = tag_escape($itemtag);
//     $captiontag = tag_escape($captiontag);
//     $columns = intval($columns);
//     $itemwidth = $columns > 0 ? floor(100/$columns) : 100;
//     $float = is_rtl() ? 'right' : 'left';

//     $selector = "gallery-{$instance}";

//     $output = apply_filters('gallery_style', "
//         <style type='text/css'>
//             #{$selector} {
//                 margin: auto;
//             }
//             #{$selector} .gallery-item {
//                 float: {$float};
//                 margin-top: 10px;
//                 text-align: center;
//                 width: {$itemwidth}%;           }
//             #{$selector} img {
//                 border: 2px solid #cfcfcf;
//             }
//             #{$selector} .gallery-caption {
//                 margin-left: 0;
//             }
//         </style>
//         <!-- see gallery_shortcode() in wp-includes/media.php -->
//         <div id='$selector' class='gallery galleryid-{$id}'>");

//     $i = 0;
//     foreach ( $attachments as $id => $attachment ) {
//         $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);

//         $output .= "<{$itemtag} class='gallery-item'>";
//         $output .= "
//             <{$icontag} class='gallery-icon'>
//                 $link
//             </{$icontag}>";
//         if ( $captiontag && trim($attachment->post_excerpt) ) {
//             $output .= "
//                 <{$captiontag} class='gallery-caption'>
//                 " . wptexturize($attachment->post_excerpt) . "
//                 </{$captiontag}>";
//         }
//         $output .= "</{$itemtag}>";
//         if ( $columns > 0 && ++$i % $columns == 0 )
//             $output .= '<br style="clear: both" />';
//     }

//     $output .= "
//             <br style='clear: both;' />
//         </div>\n";

//     return $output;
// }



//Disable update notification for individual plugins - see my example of plugin block-spam-by-math-reloaded as to how to use this function
function filter_plugin_updates($value)
{
	// if( isset($value) )
	// 	unset( $value->response['event-list/event-list.php'] );
	// return $value;
}
// add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );
add_filter('show_admin_bar', '__return_false');
add_filter('wp_image_editors', 'change_graphic_lib');
function change_graphic_lib($array)
{
	return array('WP_Image_Editor_GD', 'WP_Image_Editor_Imagick');
}


do_action('wp_ajax_nopriv_ajax-gettax');
do_action('wp_ajax_ajax-gettax');
add_action('wp_ajax_nopriv_ajax-gettax', 'ajax_gettax');
add_action('wp_ajax_ajax-gettax', 'ajax_gettax');
function ajax_gettax()
{
	$taxonomySlug = $_POST['taxonomySlug'];
	$taxonomyId = get_term_by('slug', $taxonomySlug, 'location');
	$taxonomyId = $taxonomyId->term_id;
	$args = array(
		'taxonomy' 	=> 'location',
		'hide_empty' => false,
		'parent'	=> $taxonomyId,
		'orderby'	=> 'name',
	);
	$terms = get_terms($args);

	$parsedterms = [];
	foreach ($terms as $key => $value) {
		$parsedterms[] = ['id' => $value->slug, 'name' => $value->name];
	}

	header("Content-Type: application/json");
	echo json_encode($parsedterms);
	exit;
}


do_action('wp_ajax_nopriv_ajax-gethotelbytax');
do_action('wp_ajax_ajax-gethotelbytax');
add_action('wp_ajax_nopriv_ajax-gethotelbytax', 'ajax_gethotelbytax');
add_action('wp_ajax_ajax-gethotelbytax', 'ajax_gethotelbytax');
function ajax_gethotelbytax()
{
	$taxonomySlug = $_REQUEST['taxonomySlug'];
	$page = $_REQUEST['page'];
	$args = [
		'post_type' => 'hotel',
		'posts_per_page' => 10,
		'paged' => $page,
		'tax_query' => array(
			array(
				'taxonomy' => 'location',
				'field'    => 'slug',
				'terms'    => $taxonomySlug,
			),
		),
	];
	$q = new WP_Query($args);

	$parseddata = [
		[
			'slug' => $taxonomySlug,
			'currentpage' => (int)$page,
			'maxnumpages' => $q->max_num_pages
		]
	];
	foreach ($q->posts as $key => $value) {
		$postid = $value->ID;
		$location = get_the_terms($postid, 'location');
		$location = array_reverse(wp_list_pluck($location, 'name'));
		array_pop($location);
		$location = implode(', ', $location);
		$mydata = [
			'id' => $value->post_name,
			'name' => $value->post_title,
			'image' => get_the_post_thumbnail($postid, 'newlayc'),
			'location' => $location,
			'address' => get_field('address', $postid),
			'phone' => get_field('phone', $postid),
			'site' => get_field('site', $postid),
			'permalink' => get_permalink($postid),
			'excerpt' => get_the_excerpt($postid)
		];
		$parseddata[] = $mydata;
	}

	header("Content-Type: application/json");
	echo json_encode($parseddata);
	exit;
}


add_filter('wpseo_primary_term_taxonomies', '__return_empty_array');

// if (function_exists('acf_add_options_page')) {
// 	acf_add_options_page(['page_title'=>'Banner Publicidade']);
// }
