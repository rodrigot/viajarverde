<?php get_header() ?>
<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args=array(
	'post_type' => 'post',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'ignore_sticky_posts'=> 1,
	'category__not_in'=>array(41,42,43,62),
	'posts_per_page' => 10,
	'paged' => $paged,
);
query_posts($args);
?>
<?php //get_template_part( 'sections/featured-area'); ?>

<div id="content" class="container site-content">

	<?php global $vce_sidebar_opts; ?>
	<?php if ( $vce_sidebar_opts['use_sidebar'] == 'left' ) { get_sidebar(); } ?>
	<h2 class="page-title"><?php the_title() ?></h2>

	<div id="primary" class="vce-main-content">

		<div class="main-box main-box-half">


		<?php get_template_part( 'sections/archive-title' ); ?>

			<div class="main-box-inside">
			<?php if ( have_posts() ) : ?>

				<?php $cat_posts = vce_get_category_layout();?>
				<?php
				// $the_query = new WP_Query( 'cat=-1,-41,-42,-43' );
				?>
				<?php $i = 0; while ( have_posts() ) : the_post(); $i++;?>
					<?php echo vce_loop_wrap_div($cat_posts, $i, count( $wp_query->posts )); ?>

						<?php get_template_part( 'sections/loops/layout-cwithexcerpt' ); ?>

					<?php if ( $i == ( count( $wp_query->posts ) ) ) : ?>
						</div>
					<?php endif;?>

				<?php endwhile; ?>

				<?php get_template_part( 'sections/pagination/'.vce_get_category_pagination() ); ?>

			<?php else: ?>

				<?php get_template_part( 'sections/content-none'); ?>

			<?php endif; ?>
			</div>
		</div>
	</div>
	<?php if ( $vce_sidebar_opts['use_sidebar'] == 'right' ) { get_sidebar(); } ?>
</div>
<?php get_footer(); ?>