<form class="vce-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
	<input name="s" class="vce-search-input" size="20" type="text" placeholder="<?php echo __vce('search_form'); ?>" />
	<button type="submit" class="vce-search-submit"><i class="fa fa-search"></i></button>
</form>