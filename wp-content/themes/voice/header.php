<!DOCTYPE html>
<!--[if IE 8]><html class="ie8"><![endif]-->
<!--[if IE 9]><html class="ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
<![endif]-->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<!-- <link rel="alternate" hreflang="pt-br" href="<?php echo home_url() ?>" /> -->
<?php $p = get_stylesheet_directory_uri()."/images/favicon/" ?>
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $p ?>apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $p ?>apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $p ?>apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $p ?>apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $p ?>apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $p ?>apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $p ?>apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $p ?>apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="<?php echo $p ?>favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="<?php echo $p ?>favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="<?php echo $p ?>favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo $p ?>favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="<?php echo $p ?>favicon-128.png" sizes="128x128" />
<meta name="application-name" content="Viajar Verde"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="<?php echo $p ?>mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="<?php echo $p ?>mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="<?php echo $p ?>mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="<?php echo $p ?>mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="<?php echo $p ?>mstile-310x310.png" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="vce-main">

<header id="header" class="main-header">
<?php if(vce_get_option('top_bar')) : ?>
	<?php get_template_part('sections/headers/top'); ?>
<?php endif; ?>
<?php get_template_part('sections/headers/header-'.vce_get_option('header_layout')); ?>
</header>

<div id="main-wrapper">