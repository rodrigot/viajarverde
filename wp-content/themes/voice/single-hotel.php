<?php get_header(); ?>
<div id="content" class="container site-content">
	<?php global $vce_sidebar_opts; ?>
	<?php if ($vce_sidebar_opts['use_sidebar'] == 'left') {
		get_sidebar();
	} ?>
	<div id="primary" class="vce-main-content">
		<main id="main" class="main-box main-box-single">

			<article id="post-<?php the_ID(); ?>" <?php post_class('vce-single'); ?>>
				<a href="/hoteis-sustentaveis" class="voltar">&laquo; voltar</a>
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>

				<?php if (vce_get_option('show_fimg') && has_post_thumbnail()) : ?>
					<?php if (!vce_is_paginated_post() || (vce_is_paginated_post() && vce_get_option('show_paginated_fimg')) && $page <= 1) : ?>

						<?php global $vce_sidebar_opts;
						$img_size = $vce_sidebar_opts['use_sidebar'] == 'none' ? 'vce-lay-a-nosid' : 'vce-lay-a'; ?>

						<div class="meta-image">
							<?php
							$id = get_post_thumbnail_id();
							$featAlt = trim(strip_tags(get_post_meta($id, '_wp_attachment_image_alt', true)));
							$featTitle = trim(strip_tags(get_post_meta($id, '_wp_attachment_image_alt', true)));
							$attrs = array(
								'alt' => $featAlt,
								'title' => $featTitle,
								'class' => 'pinthis'
							);
							$featuredimg = get_the_post_thumbnail(get_the_ID(), $img_size, $attrs);
							$pinDescr = !empty($featTitle) ? $featTitle : $featAlt;
							if (function_exists('pibfi_engine_add_pin'))
								echo pibfi_engine_add_pin($featuredimg, 'http://pinterest.com/pin/create/button/', get_permalink(), $pinDescr);
							else
								echo $featuredimg
							?>

							<?php if (vce_get_option('show_fimg_cap') && $caption = get_post(get_post_thumbnail_id())->post_excerpt) : ?>
								<div class="vce-photo-caption"><?php echo $caption;  ?></div>
							<?php endif; ?>
						</div>

						<?php if (vce_get_option('show_author_img')) : ?>
							<div class="meta-author">
								<?php echo get_avatar(get_the_author_meta('ID'), 100); ?>
								<div class="meta-author-wrapped"><?php echo __vce('written_by'); ?> <span class="vcard author"><span class="fn"><a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span></span></div>
							</div>
						<?php endif; ?>

					<?php endif; ?>
				<?php endif; ?>

				<?php $galeria = get_field('gallery'); ?>
				<?php if (!empty($galeria)) : ?>
					<div class="galeria">
						<div class="slides">
							<?php foreach ($galeria as $value) : ?>
								<div class="slide">
									<a href="<?php echo wp_get_attachment_image_url($value, 'large') ?>" data-fancybox='gallery'>
										<?php echo wp_get_attachment_image($value, 'thumbnail') ?>
									</a>
								</div>
							<?php endforeach ?>
						</div>
					</div>
				<?php endif ?>

				<?php
				$terms = get_the_terms(get_the_ID(), 'sustentabilidade');
				if (!empty($terms)) : ?>
					<div class="sustentabilidade">
						<ul>
							<?php foreach ($terms as $term) : ?>
								<li>
									<?php if (function_exists('z_taxonomy_image')) : ?>
										<figure><?php z_taxonomy_image($term->term_id, 'thumbnail') ?></figure>
									<?php endif; ?>
									<div><?php echo $term->name ?></div>
								</li>
							<?php endforeach ?>
						</ul>
					</div>
				<?php endif ?>


				<div class="hotel-meta">
					<div class="info">
						<div class="address"><?php the_field('address') ?></div>
						<div class="phone"><?php the_field('phone') ?></div>
						<div class="site">
							<a href="<?php the_field('site') ?>" target="_blank">
								<?php echo str_replace(['http://', 'https://'], '', get_field('site')) ?>
							</a>
						</div>
					</div>
				</div>

				<div class="entry-content">
					<?php the_content(); ?>
				</div>


		</main>
	</div>
	<?php if ($vce_sidebar_opts['use_sidebar'] == 'right') {
		get_sidebar();
	} ?>
</div>
<?php get_footer(); ?>