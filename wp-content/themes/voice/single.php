<?php get_header(); ?>

<div id="content" class="container site-content">

	<?php global $vce_sidebar_opts; ?>
	<?php if ($vce_sidebar_opts['use_sidebar'] == 'left') {
		get_sidebar();
	} ?>

	<div id="primary" class="vce-main-content">

		<main id="main" class="main-box main-box-single">

			<?php get_template_part('sections/part-banner-publicidade') ?>

			<?php while (have_posts()) : the_post(); ?>
				<?php get_template_part('sections/content', get_post_format()); ?>
			<?php endwhile; ?>
			<!--<div class="gst_banner" style="text-align: center;display: table;margin: auto;border: 1px solid black">
<a href="https://www.gstcouncil.org/gstc2017aysen/gstc2017aysen-espanol/" target="_blank"><img src="http://viajarverde.com.br/wp-content/uploads/2017/06/gstcouncil_banner.jpg" /></a>
</div>-->
			<?php if (vce_get_option('show_prev_next')) : ?>
				<?php get_template_part('sections/prev-next'); ?>
			<?php endif; ?>

		</main>

		<?php if (vce_get_option('show_author_box') && vce_get_option('author_box_position') == 'up') : ?>
			<?php get_template_part('sections/author-box'); ?>
		<?php endif; ?>

		<?php comments_template(); ?>

		<?php if (vce_get_option('show_related')) : ?>
			<?php get_template_part('sections/related-box'); ?>
		<?php endif; ?>

		<?php if (vce_get_option('show_author_box') && vce_get_option('author_box_position') == 'down') : ?>
			<?php get_template_part('sections/author-box'); ?>
		<?php endif; ?>


	</div>

	<?php if ($vce_sidebar_opts['use_sidebar'] == 'right') {
		get_sidebar();
	} ?>

</div>

<?php get_footer(); ?>