<?php
/**
 * Template Name: Bilingue
 */
?>
<?php get_header(); ?>

<div id="content" class="container site-content">

	<?php global $vce_sidebar_opts; ?>
	<?php if ( $vce_sidebar_opts['use_sidebar'] == 'left' ) { get_sidebar(); } ?>

	<div id="primary" class="vce-main-content">

		<main id="main" class="main-box main-box-single">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class('vce-page'); ?>>

				<div class="entry-content page-content">

					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist" id="myTabs">
						<li role="presentation" class="active"><a href="#pt" aria-controls="pt" role="tab" data-toggle="tab"><?php the_field("label_pt")?> <span>pt</span></a></li>
						<li role="presentation"><a href="#en" aria-controls="en" role="tab" data-toggle="tab"><?php the_field("label_en")?> <span>en</span></a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content glossario">
						<div role="tabpanel" class="tab-pane active" id="pt"><?php the_content(); ?></div>
						<div role="tabpanel" class="tab-pane" id="en"><?php the_field("content_en")?></div>
					</div>

					<?php the_field("conteudo_adicional"); ?>
				</div>

			</article>

		<?php endwhile; ?>

		</main>

		<?php wpp_get_mostpopular( 'header="Popular Posts"&header_start="<h2 class=wpplist_title>"&header_end="</h2>"&limit=4&thumbnail_width=153&thumbnail_height=115&stats_views=0&post_type="post"' ); ?>

		<div id="templatebilingue_ad" class="myadspace"><?php dynamic_sidebar('adbar_responsive'); ?></div>

	</div>

	<?php if ( $vce_sidebar_opts['use_sidebar'] == 'right' ) { get_sidebar(); } ?>

</div>

<?php get_footer(); ?>