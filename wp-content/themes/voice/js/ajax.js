function getTaxonomyChildrenBySlug(taxonomySlug, callback) {
	jQuery.post(
		MyAjax.ajaxurl,
		{
			action: "ajax-gettax",
			taxonomySlug: taxonomySlug,
		},
		function (response) {
			if (callback && typeof callback === "function") callback(response);
		}
	);
}

function getHotelsByTaxonomy(taxonomySlug, callback, page = 1) {
	jQuery.post(
		MyAjax.ajaxurl,
		{
			action: "ajax-gethotelbytax",
			taxonomySlug: taxonomySlug,
			page: page,
		},
		function (response) {
			if (callback && typeof callback === "function") callback(response);
		}
	);
}
