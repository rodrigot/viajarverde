<?php get_header(); ?>
<div id="content" class="container site-content">
	<?php global $vce_sidebar_opts; ?>
	<?php if ( $vce_sidebar_opts['use_sidebar'] == 'left' ) { get_sidebar(); } ?>
	<div id="primary" class="vce-main-content">
		<main id="main" class="main-box main-box-single">
		<?php //while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class('vce-page'); ?>>
				<header class="entry-header">
					<h1 class="entry-title entry-title-page"><?php the_title(); ?></h1>
				</header>
				<div class="entry-content page-content">
				<?php $mycat = new WP_Query('category_name=rapidinhas'); ?>

				<?php while ($mycat->have_posts()) : $mycat->the_post(); ?>
					<div class="rapidinhas-entry"><span class="rapidinhas-date"><?php echo get_the_date()?></span> <a href="<?php the_permalink() ?>"><?php the_title() ?></a></div>
				<?php endwhile; ?>

				</div>
			</article>

		<?php //endwhile; ?>
		</main>
	</div>
	<?php if ( $vce_sidebar_opts['use_sidebar'] == 'right' ) { get_sidebar(); } ?>
</div>
<?php get_footer(); ?>