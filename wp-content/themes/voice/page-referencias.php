<?php get_header(); ?>

<div id="content" class="container site-content">

	<?php global $vce_sidebar_opts; ?>
	<?php if ( $vce_sidebar_opts['use_sidebar'] == 'left' ) { get_sidebar(); } ?>

	<div id="primary" class="vce-main-content">

		<main id="main" class="main-box main-box-single">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class('vce-page'); ?>>

				<div class="entry-content page-content">

					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist" id="myTabs">
						<li role="presentation" class="active"><a href="#glossario" aria-controls="glossario" role="tab" data-toggle="tab">Glossário <span>pt</span></a></li>
						<li role="presentation"><a href="#glossary" aria-controls="glossary" role="tab" data-toggle="tab">Glossary <span>en</span></a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content glossario">
						<div role="tabpanel" class="tab-pane active" id="glossario"><?php the_content(); ?></div>
						<div role="tabpanel" class="tab-pane" id="glossary"><?php the_field("glossary_eng")?></div>
					</div>

					<?php the_field("bibliografia"); ?>
				</div>

			</article>

		<?php endwhile; ?>

		</main>

	</div>

	<?php if ( $vce_sidebar_opts['use_sidebar'] == 'right' ) { get_sidebar(); } ?>

</div>

<?php get_footer(); ?>