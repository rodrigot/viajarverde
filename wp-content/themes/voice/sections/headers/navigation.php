<div id="sticky_header" class="header-sticky">
	<div class="container">
		<div class="inner-container">
			<a class="vce-responsive-nav" href="#sidr-main"><i class="fa fa-bars"></i></a>
			<div class="main-navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'vce_main_navigation_menu', 'menu' => 'vce_main_navigation_menu', 'menu_class' => 'nav-menu vce_main_navigation_menu', 'menu_id' => 'vce_main_navigation_menu', 'container' => false ) ); ?>
				<div class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Viajar Verde">Viajar Verde</a></div>
				<?php wp_nav_menu( array( 'theme_location' => 'vce_main_navigation_menu_2', 'menu' => 'vce_main_navigation_menu_2', 'menu_class' => 'nav-menu vce_main_navigation_menu_2', 'menu_id' => 'vce_main_navigation_menu_2', 'container' => false ) ); ?>
			</div>
		</div>
	</div>
</div>


<nav id="site-navigation" class="main-navigation" role="navigation">
	<?php
		if(has_nav_menu('vce_main_navigation_menu')) {
				wp_nav_menu( array( 'theme_location' => 'vce_main_navigation_menu', 'menu' => 'vce_main_navigation_menu', 'menu_class' => 'nav-menu vce_main_navigation_menu', 'menu_id' => 'vce_main_navigation_menu', 'container' => false ) );
				wp_nav_menu( array( 'theme_location' => 'vce_main_navigation_menu_2', 'menu' => 'vce_main_navigation_menu_2', 'menu_class' => 'nav-menu vce_main_navigation_menu_2', 'menu_id' => 'vce_main_navigation_menu_2', 'container' => false ) );
		} else { ?>
			<ul id="vce_header_nav" class="nav-menu"><li>
				<a href="<?php echo admin_url('nav-menus.php'); ?>"><?php _e('Click here to add navigation menu', THEME_SLUG); ?></a>
			</li></ul>
<?php }	?>
</nav>

<div id="category-navigation-holder">
<nav id="category-navigation" class="hidden-lg">
	<aside class="sidebar">
	<?php dynamic_sidebar('vce_mobile_sticky_sidebar'); ?>
	</aside>
</nav>
</div>