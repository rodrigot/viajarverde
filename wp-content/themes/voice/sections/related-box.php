<?php $related_posts = vce_get_related_posts(); ?>

<?php if(!empty($related_posts) && $related_posts->have_posts()): ?>

	<div class="main-box vce-related-box">

	<h3 class="main-box-title"><?php echo __vce('related_title'); ?></h3>

	<div class="main-box-inside">

		<?php while($related_posts->have_posts()): $related_posts->the_post(); ?>
			<?php //get_template_part('sections/loops/layout', vce_get_option('related_layout')); ?>

		<article <?php post_class('vce-post vce-lay-d'); ?>>

		 	<?php if($fimage = vce_featured_image('vce-lay-d')): ?>
			 	<div class="meta-image">
					<a href="<?php echo esc_url(get_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
						<?php echo $fimage; ?>
						<?php if($icon = vce_post_format_icon('lay_d')) :?>
							<span class="vce-format-icon">
							<i class="fa <?php echo $icon; ?>"></i>
							</span>
						<?php endif; ?>
					</a>
				</div>
			<?php endif; ?>

			<header class="entry-header">
				<?php if( vce_get_option('lay_d_cat')) : ?>
					<span class="meta-category"><?php echo vce_get_category(); ?></span>
				<?php endif; ?>
				<h2 class="entry-title"><a href="<?php echo esc_url(get_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php echo vce_get_title('lay-d'); ?></a></h2>
				<div class="entry-meta">
					<?php echo vce_get_meta_data('lay-d'); ?>
					<?php echo '<span class="vcard author"><span class="fn">'.__vce( 'by_author' ).' <a href="'.get_author_posts_url( get_the_author_meta( 'ID' ) ).'">'.get_the_author_meta( 'display_name' ).'</a></span></span>';?>
				</div>
			</header>

		</article>


		<?php endwhile; ?>

		<?php wp_reset_postdata(); ?>

	</div>

	</div>

<?php endif; ?>


