<article <?php post_class('vce-post vce-lay-f'); ?>>

	<header class="entry-header">
		<h2 class="entry-title"><a href="<?php echo esc_url(get_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php echo vce_get_title('lay-f'); ?></a></h2>
		<div class="entry-meta"><span class="updated"><?php echo vce_get_date()?></span></div>
	</header>

</article>
