<article <?php post_class('vce-post vce-lay-c'); ?>>

	<?php if($fimage = vce_featured_image(array(375,260,true))):
		$fimage = wp_get_attachment_image( get_post_thumbnail_id(), 'newlayc' );

	?>
	 	<div class="meta-image">
			<a href="<?php echo esc_url(get_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
				<?php echo $fimage; ?>
				<?php if($icon = vce_post_format_icon('lay_c')) :?>
					<span class="vce-format-icon">
					<i class="fa <?php echo $icon; ?>"></i>
					</span>
				<?php endif; ?>
			</a>
			<header class="entry-header">
				<?php if( vce_get_option('lay_c_cat')) : ?>
					<span class="meta-category vce-featured-section"><?php echo vce_get_category(); ?></span>
				<?php endif; ?>
				<h2 class="entry-title"><a href="<?php echo esc_url(get_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php echo vce_get_title('lay-c'); ?></a></h2>
				<div class="entry-meta"><?php echo vce_get_meta_data('lay-c'); ?></div>
				<div class="excerpt">
					<?php the_excerpt();?>
					<a href="<?php the_permalink();?>" class="readmore">Leia Mais <i class="fa fa-angle-right"></i></a>
				</div>
			</header>
		</div>
	<?php endif; ?>


<!--
	<?php if( vce_get_option('lay_c_excerpt')) : ?>
		<div class="entry-content">
			<p><?php echo vce_get_excerpt('lay-c'); ?></p>
		</div>
	<?php endif; ?>
-->
</article>