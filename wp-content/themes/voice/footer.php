<footer id="footer" class="site-footer">
	<div id="inner-footer">
		<div class="social">
			<ul>
				<li class="instagram"><a href="<?php the_field('instagram', 207) ?>"><i class="icon-instagram"></i></a></li>
				<li class="facebook"><a href="<?php the_field('facebook', 207) ?>"><i class="icon-facebook"></i></a></li>
				<li class="twitter"><a href="<?php the_field('twitter', 207) ?>"><i class="icon-twitter"></i></a></li>
			</ul>
		</div>
		<div class="parceiros">
			<ul>
				<?php
				$parceiros = get_field("parceiros", 207);
				foreach ($parceiros as $item) :
					$img = wp_get_attachment_image($item['imagem'], 'full');
					$link = $item['link'];
					$output = empty($link) ? $img : sprintf("<a href='http://%s'>%s</a>", $link, $img);
				?><li><?php echo $output ?></li>
				<?php endforeach ?></ul>
		</div>
		<div class="signature">
			<div class="selo"></div>
			<span>© VIAJAR VERDE 2015-<?php echo Date("Y") ?><br/>Todos os direitos reservados</span>
			<a href="" class="sig"></a>
		</div>
	</div>
</footer>
</div>
</div>
<?php if (vce_get_option('scroll_to_top')) : ?>
	<a href="javascript:void(0)" id="back-top"><i class="fa fa-angle-up"></i></a>
<?php endif; ?>
<?php wp_footer(); ?>
<script type="text/javascript">
	if (typeof wabtn4fg === "undefined") {
		wabtn4fg = 1;
		h = document.head || document.getElementsByTagName("head")[0], s = document.createElement("script");
		s.type = "text/javascript";
		s.src = "<?php echo get_template_directory_uri() ?>/js/whatsapp-button.js";
		h.appendChild(s);
	}
</script>
</body>
</html>