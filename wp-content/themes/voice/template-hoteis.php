<?php

/**
 * Template Name: Hoteis Sustentaveis
 */
?>
<?php get_header() ?>
<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
	'post_type' => 'hotel',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	// 'ignore_sticky_posts'=> 1,
	// 'category__not_in'=>array(41,42,43,62),
	// 'posts_per_page' => 10,
	// 'paged' => $paged,
);
query_posts($args);
?>

<div id="content" class="container site-content">

	<?php global $vce_sidebar_opts; ?>
	<?php if ($vce_sidebar_opts['use_sidebar'] == 'left') {
		get_sidebar();
	} ?>
	<h2 class="page-title"><?php the_title() ?></h2>

	<div id="primary" class="vce-main-content">
		<main id="main" class="main-box main-box-single">
			<?php the_content() ?>

			<?php
			$args = array(
				'taxonomy' => 'location',
				'hide_empty' => false,
				'parent'	=> '0',
				'orderby'	=> 'name',
				'order'		=> 'ASC',
			);
			$terms = get_terms($args);
			?>

			<h3>Pesquise aqui seu hotel:</h3>
			<div class="searchbylocation">
				<form id="searchbylocation" method="get" action="<?php bloginfo('url'); ?>">
					<input type="text" name="s" value="" placeholder="search&hellip;" maxlength="50" />
					<div class="local-wrapper">
						<select name="local0" class="local0">
							<option value="" selected="selected">Selecione o país</option>
							<?php foreach ($terms as $term) : ?>
								<option value="<?php echo $term->slug ?>"><?php echo $term->name ?></option>
							<?php endforeach ?>
						</select>
					</div>
					<input type="button" class="submit search-hotel-button" value="Buscar">
					<div class="loader">Carregando...</div>
				</form>
			</div>

			<div class="hotel-list-wrapper">
				<div class="hotel-list"></div>
			</div>
		</main>
	</div>
	<?php if ($vce_sidebar_opts['use_sidebar'] == 'right') {
		get_sidebar();
	} ?>
</div>
<?php get_footer(); ?>