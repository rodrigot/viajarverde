<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

//Begin Really Simple SSL session cookie settings
@ini_set('session.cookie_httponly', true);
@ini_set('session.cookie_secure', true);
@ini_set('session.use_only_cookies', true);
//END Really Simple SSL

/**
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'viajarve_viajarverde');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'viajarve_dbuser');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'xwx#ZUrxFCu,');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N).(`-xI=iI%S=-1wv9b*%07xVlXIqD-(NKMi6p|Vr%T`N9gW8$+|,l0faw[s( G');
define('SECURE_AUTH_KEY',  'C28}EGCmNUAMxh0jsSb>z-33[`NZzv:N*dDN#7a?l`NDbG D+{t-HnYtw/?Pydh$');
define('LOGGED_IN_KEY',    'klHIWHS24s3,a|(iqoMnz#->qgGxoA~(op ~I/r(jsQ;@.N+f,-vWnmR@PV&zLwn');
define('NONCE_KEY',        'ASXmTxVm&ZpdQ<18zA1m_41ylH->A4O(!(-4YNHe iFk$XW%b]rqUwq7_%B{YuTo');
define('AUTH_SALT',        'rcmJ9Kt=`U/Z.-)bM|gWUu+%D#F>4ZV5lSs(T.xp$td8|S@I01g;NwL.WUf4$lln');
define('SECURE_AUTH_SALT', ';6|LWR{IwMCae+Q#HF<@bA|l`33`b,GSv.a|av4D*H+xY=PF81Q*}0r1*.[D2 Br');
define('LOGGED_IN_SALT',   '|O@hil]el21GQR,S3RozL`g*sr%~!4lClSl;>`5ev(%dnNZIO?@9{6hAn)|Yu7m;');
define('NONCE_SALT',       'woC^u|<~7iXk`H4:r0qEt_R+6~Y)U8=-82W|p|2h|qlC={MeV9;O-4#]oG?g8e-y');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'vv_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
